package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utils.ReadExcel;
import wdMethods.ProjectMethod;

public class TC001CreateLead extends ProjectMethod {

	@BeforeTest(groups="smoke")
	public void setData() {
		testCaseName = "TC001CreateLead";
		testCaseDiscription = "Creating the test cases";
		author = "Siva";
		category = "smoke";
	}

	//@Test(invocationCount=2, invocationTimeOut=4000)
	//@Test(groups= {"smoke"})
	@Test(groups={"smoke"},dataProvider="QA")
	public void createlead(String comName,String fName,String lName ) {
		//login(); //Because it mentioned as @BeforeMethod in Project Class
		WebElement cLead = locateElement("xpath", "//a[text()='Create Lead']");
		click(cLead);
		WebElement cName = locateElement("id", "createLeadForm_companyName");
		//type(cName, "HCL");
		type(cName,comName);
		WebElement FName = locateElement("xpath", "//input[@id='createLeadForm_firstName']");
		//type(FName, "Siva");
		type(FName,fName);
		WebElement LName = locateElement("id", "createLeadForm_lastName");
		//type(LName, "R");
		type(LName,lName);
		WebElement sDD = locateElement("xpath", "//select[@id='createLeadForm_dataSourceId']");
		selectDropDownUsingText(sDD, "Conference");
		WebElement Flocal = locateElement("xpath", "//input[@name='firstNameLocal']");
		type(Flocal, "Raj");
		//String parentWin=driver.getWindowHandle();
		//String title = driver.getTitle();
		//System.out.println("GGGGGGGGGGGG"+title);
		WebElement industryName = locateElement("xpath", "//select[@name='industryEnumId']");
		selectDropDownUsingText(industryName, "Aerospace");
		WebElement ownerShip = locateElement("xpath","//select[@class='inputBox' and @id='createLeadForm_ownershipEnumId']");
		selectDropDownUsingText(ownerShip, "Partnership");
		WebElement parentAcc = locateElement("xpath", "//img[@alt='Lookup']");
		click(parentAcc);
		switchToWindow(1);
		WebElement accListSelect = locateElement("xpath", "//a[text()='Company']");
		clickWithoutSnap(accListSelect);
		switchToWindow(0);
		//Thread.sleep(300);
		String cWin = driver.getTitle();
		System.out.println(cWin);
		//	driver.switchTo().window(parentWin);//No Need its handled by swtichWindow Method
		WebElement calBirth = locateElement("xpath", "//img[@alt='View Calendar']");
		clickWithoutSnap(calBirth);
		WebElement selectDate = locateElement("xpath","//table/parent::div[@class='calendar']");
		table(selectDate,"//tr[@class='daysrow']", "17");
		WebElement clButton = locateElement("Name", "submitButton");
		click(clButton);
	}
	@DataProvider(name="QA")
	public Object[][] fetchLead() throws IOException
	{
		Object[][] data = ReadExcel.fetchExcel();
		/*
		String data[][]=new String[1][3];
		data[0][0]="Google";
		data[0][1]="Siva";
		data[0][2]="R";
*/
/*		data[1][0]="Yahoo";
		data[1][1]="Anu";
		data[1][2]="R";
		
		data[2][0]="FB";
		data[2][1]="Sai Raksha";
		data[2][2]="S";		*/
		return data;
		
	}
}
