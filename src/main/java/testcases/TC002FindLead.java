package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethod;

public class TC002FindLead extends ProjectMethod {
	@BeforeTest(groups="smoke")
	public void setData() {
		testCaseName = "TC002FindLead";
		testCaseDiscription = "Find Lead test cases";
		author = "Siva";
		category = "smoke";
	}
	//@Test(dependsOnMethods="testcases.TC001CreateLead.createlead", alwaysRun=true)
	@Test(groups={"sanity"},dependsOnGroups= {"smoke"})
	public void findLeads() {
		//login(); //Because it mentioned as @BeforeMethod in Project Class
		WebElement eleLeads = locateElement("LinkText", "Leads");
		click(eleLeads);
		WebElement findLead = locateElement("LinkText", "Find Leads");
		click(findLead);
		WebElement firstNameA = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(firstNameA, "Nagarajan");
		WebElement lastNameA = locateElement("xpath", "(//input[@name='lastName'])[3]");
		type(lastNameA, "Ramakrishnan");
		WebElement companyNameA = locateElement("xpath", "(//input[@name='companyName'])[2]");
		type(companyNameA, "Volante Technologies");
		WebElement buttonA = locateElement("xpath", "(//button[@class='x-btn-text'])[7]");
		click(buttonA);
	}

}
