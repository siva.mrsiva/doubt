package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import wdMethods.ProjectMethod;

public class TC003MergeLead extends ProjectMethod {
	
	@BeforeTest(groups="smoke")
	public void setData() {
		testCaseName="TC003MergeLead";
		testCaseDiscription="Merge two diff leads";
		author="siva";
		category="smoke";
	}

@Test(groups={"regression"},dependsOnGroups= {"sanity"})
public void mergeLead() throws InterruptedException
{
	WebElement leadLink = locateElement("LinkText","Leads");
	click(leadLink);
	WebElement mergeLead = locateElement("LinkText","Merge Leads");
	click(mergeLead);
	WebElement fromLeadSelection = locateElement("xpath","//img[@alt='Lookup']");//Not Working
	click(fromLeadSelection);
	switchToWindow(1);
	WebElement firstName = locateElement("xpath","//input[@name='firstName']");
	type(firstName, "Siva");
	WebElement clickFlead = locateElement("xpath","//button[text()='Find Leads']");
	click(clickFlead);
	Thread.sleep(2000);
	WebElement selectCLEAD = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
	clickWithoutSnap(selectCLEAD);
	switchToWindow(0);
	WebElement toLeadSelection = locateElement("xpath","(//img[@alt='Lookup'])[2]");//Not Working
	click(toLeadSelection);
	switchToWindow(1);
	WebElement first2Name = locateElement("xpath","//input[@name='firstName']");
	type(first2Name, "Babu");
	WebElement click2Flead = locateElement("xpath","//button[text()='Find Leads']");
	click(click2Flead);
	Thread.sleep(2000);
	WebElement select2CLEAD = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
	clickWithoutSnap(select2CLEAD);
	switchToWindow(0);
	WebElement mergerButton = locateElement("class","buttonDangerous");
	clickWithoutSnap(mergerButton);
	getAlertText();
	dismissAlert();	
}
	

}
