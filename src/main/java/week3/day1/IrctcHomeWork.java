package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IrctcHomeWork {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.findElementById("userRegistrationForm:userName").sendKeys("siva124456");
		driver.findElementByLinkText("Check Availability").click();
		Thread.sleep(2000);
		String matAvail = driver.findElementById("userRegistrationForm:useravail").getText();
		System.out.println(matAvail);
		
	//if (matAvail=="User Id is Available..Please go ahead with the Registration Process...")
		if (matAvail.contains("User Id is Available..Please go ahead with the Registration Process...")){
			System.out.println("Valid UserName is available");
			driver.findElementById("userRegistrationForm:password").sendKeys("1234");
			driver.findElementById("userRegistrationForm:confpasword").sendKeys("1234");
			// Security dropdown option start
			WebElement secdd = driver.findElementById("userRegistrationForm:securityQ");
			Select secopt = new Select(secdd);
			secopt.selectByVisibleText("What make was your first car or bike?");
			// Security dropdown option seletion option is finished
			driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("i20");
			// Preffered lang dropdown option start
			WebElement Prefferedlang = driver.findElementById("userRegistrationForm:prelan");
			Select selprefer = new Select(Prefferedlang);
			selprefer.selectByValue("hi");
			List<WebElement> options = selprefer.getOptions();
			int size = options.size();
			System.out.println(size);
			for (WebElement countofOpt : options) {
				System.out.println(countofOpt.getText());
			}
			// Preffered lang dropdown delection optioned finished
			driver.findElementById("userRegistrationForm:firstName").sendKeys("sivaraj");
			driver.findElementById("userRegistrationForm:gender:1").click();
			WebElement ddCount = driver.findElementById("userRegistrationForm:countries");
	        Select ddOpt=new Select(ddCount);
	        ddOpt.selectByVisibleText("Egypt")  ;
	        
		}
		 else {
		System.out.println("User Id already exists .... Please choose a different User Name...");
	}
	}

}
