package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLeaf {

	public static void main(String[] args) {
		
		// Invoke the driver
System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
//Create obj for chrome

ChromeDriver driver=new ChromeDriver();
driver.manage().window().maximize();
driver.get("http://leaftaps.com/opentaps");
driver.findElementById("username").sendKeys("DemoSalesManager");
driver.findElementById("password").sendKeys("crmsfa");
driver.findElementByClassName("decorativeSubmit").click();
//Click the link 
driver.findElementByLinkText("CRM/SFA").click();
//Create leaf
driver.findElementByLinkText("Create Lead").click();
//First row value
boolean enabled = driver.findElementByName("companyName").isDisplayed();
System.out.println(enabled);
driver.findElementById("createLeadForm_companyName").sendKeys("facebook");
//Second row value
driver.findElementById("createLeadForm_firstName").sendKeys("Anukkraha");
//Thrid row value
driver.findElementById("createLeadForm_lastName").sendKeys("Sivaraj");
//Click the button
//driver.findElementByName("submitButton").click();
//String text = driver.findElementById("viewLead_firstName_sp").getText();
//if (text.equals("")) {
WebElement dropdown = driver.findElementById("createLeadForm_dataSourceId");
Select dd=new Select(dropdown);
	dd.selectByVisibleText("Public Relations");
	List<WebElement> val = dd.getOptions();
	//dd.selectByIndex(2);//check the browser respective value is selected in the drop down
	//dd.selectByValue("LEAD_CONFERENCE");//check the browser respective value is selected in the drop down
	//dd.selectByVisibleText("Employee");//check the browser respective value is selected in the drop down
for (WebElement opt : val) {
System.out.println(opt.getText());
	
}
WebElement drop2 = driver.findElementById("createLeadForm_marketingCampaignId");
Select dd2=new Select(drop2);
dd2.selectByValue("CATRQ_AUTOMOBILE");

List<WebElement> val2 = dd2.getOptions();

System.out.println("==========Below are the option contaoin M ========== ");
for (WebElement opt1 : val2) {
	if(opt1.getText().contains("M"))
	 {
		 System.out.println(opt1.getText());

	}
}
	}

}
