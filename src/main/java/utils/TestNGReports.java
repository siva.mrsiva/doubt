package utils;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class TestNGReports {
	public static ExtentReports editHtml;
	public static ExtentTest logger;
	public ExtentHtmlReporter htmlReport;
	public String testCaseName, testCaseDiscription, category, author;;
	@BeforeSuite(groups={"any"})
	public void startResult() {
		htmlReport= new ExtentHtmlReporter("./REPORTS/result.html");//Not Editable By Defalut																				// non-Editable
		htmlReport.setAppendExisting(true);// Maintaining the History
		editHtml = new ExtentReports();
		editHtml.attachReporter(htmlReport);

	}

	public void reportStep(String status, String desc) {
		if (status.equalsIgnoreCase("pass")) {
			logger.log(Status.PASS, desc);
		} else if (status.equalsIgnoreCase("fail")) {
			logger.log(Status.FAIL, desc);
		}
	}

@AfterSuite(groups={"any"})
	public void endResult() {

		editHtml.flush();
	}

//@BeforeMethod
@BeforeClass(groups={"any"})
		public void beforeMethod() {		
		logger = editHtml.createTest(testCaseName,testCaseDiscription);
		logger.assignAuthor(author);
		logger.assignCategory(category);
	}

}
