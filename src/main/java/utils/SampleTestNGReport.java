package utils;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class SampleTestNGReport {

	@Test
	public void startReport() throws IOException {
		ExtentHtmlReporter htmlReport = new ExtentHtmlReporter("./REPORTS/result.html");// This stage its non-Editable
		htmlReport.setAppendExisting(true);// Maintaining the History
		ExtentReports editHtml = new ExtentReports();
		editHtml.attachReporter(htmlReport);
		// For Testcase Level
		ExtentTest logger = editHtml.createTest("TC001Creatlead", "This is used to create test case");// ("Testcase Name","Description")
		logger.assignAuthor("Siva");
		logger.assignCategory("Smoke");

		// For Test step Level
		logger.log(Status.PASS, "The Data DemoSalesManager Entered Successfully",MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());//Image will not show if its in different folder
		// ("If try success it will give pass","Description","MediaEntry used To attach
		// Image or small Video Gif","Location" Need to bulid all
		editHtml.flush();// Without this no above script will execute

	}
}
