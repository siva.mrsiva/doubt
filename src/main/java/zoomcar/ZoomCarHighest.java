package zoomcar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethod;
import wdMethods.SeMethods;

public class ZoomCarHighest extends SeMethods{
//public class ZoomCarHighest extends ProjectMethod{
	@BeforeTest
	public void setData() {
		testCaseName = "TC001ZoomCarFind";
		testCaseDiscription = "Finding Highest Car";
		author = "Siva";
		category = "smoke";
	}
	@Test
	public void findHigh()
	{
		startApp("chrome","https://www.zoomcar.com/chennai/");
		WebElement zoomSearch = locateElement("xpath","//a[text()='Start your wonderful journey']");
		click(zoomSearch);
		//locateElement("xpath","//div[@class='items'][1]");
		WebElement selPopuplar = locateElement("xpath","//div[contains(text(),'Thuraipakkam')]");
		click(selPopuplar);
		WebElement clickNext = locateElement("xpath","//button[contains(text(),'Next')]");
		click(clickNext);
		Date date=new Date();
		DateFormat sdf=new SimpleDateFormat("dd");
		String today = sdf.format(date);
		int tomorrow =Integer.parseInt(today)+1;
		String ExpectedTextForVerif = locateElement("xpath", "//div[contains(text(),'"+tomorrow+"')]").getText();
		WebElement selecTom = locateElement("xpath", "//div[contains(text(),'"+tomorrow+"')]");
		click(selecTom);		

		WebElement nextPage = locateElement("xpath","//button[@class='proceed']");
		click(nextPage);
		
	WebElement verifyDate = locateElement("xpath","//div[contains(@class,'day picked')]");	
		/*String a = str.getAttribute("class");
		System.out.println(a);*/
	verifyPartialText(verifyDate,ExpectedTextForVerif);
	WebElement next2Page = locateElement("xpath","//button[@class='proceed']");
	click(next2Page);
	List<WebElement> searchResult = locateElements("class","component-car-item");
	//int size = searchResult.size();
//System.out.println(size);
//	for (WebElement webElementList : searchResult) {
	int a=1;
	//Collection<String> values=new ArrayList<>();
	Map<String, String> obj=new HashMap<>();
	for (int i = 0; i < searchResult.size(); i++) {
		
		List<WebElement> NameElement= searchResult.get(i).findElements(By.tagName("h3"));
		String text = NameElement.get(i).getText();
		System.out.println(text);
		//******Want to know why the below line return same value since it become unique by index
		List<WebElement> priceEle = searchResult.get(i).findElements(By.xpath("//div[@class='price']"));
		//WebElement priceEle = searchResult.get(i).findElement(By.xpath("(//div[@class='price'])["+a+"]"));
	//a++;
		String text2 = priceEle.get(i).getText();
		String replaceAll = text2.replaceAll("\\D", "");
	System.out.println(replaceAll);
	obj.put(text,replaceAll);
	
	}
    String maxValueInMap=(Collections.max(obj.values()));  // This will return max value in the Hashmap
    for (Entry<String, String> entry : obj.entrySet()) {  // Itrate through hashmap
        if (entry.getValue()==maxValueInMap) {
            System.out.println("Max Price is Car name is"+entry.getKey());     // Print the key with max value
        }
    }	
    System.out.println("Max Price is "+maxValueInMap);

//String max = Collections.max(obj);
//System.out.println("Max List is "+max);

/*List<Integer> obj=new ArrayList<>();
for (int i = 0; i < searchResult.size(); i++) {
	String listOfPriceWithSpecial = searchResult.get(i).getText();
	String price = listOfPriceWithSpecial.replaceAll("\\D", "");
	//System.out.println(price);
int b=Integer.parseInt(price);
obj.add(b);
//System.out.println(obj.get(i));

}	
Integer max = Collections.max(obj);
System.out.println("Max of Car"+max);
	//closeBrowser();
	int indexOf = obj.indexOf(max);	
	System.out.println(indexOf);
	String Name = searchResult.get(13).getText();
	System.out.println("ssss"+Name);
*/	}

}
