package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import utils.TestNGReports;

public class SeMethods extends TestNGReports implements WdMethods {
	public int i = 1;
	public static  RemoteWebDriver driver;//To Access All the browser we need to set in parent level

	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			// System.out.println("The Browser " + browser + " Launched Successfully");
			reportStep("Pass", "The Browser " + browser + " Launched Successfully");
		} catch (WebDriverException e) {
			// System.out.println("The Browser " + browser + " not Launched ");
			reportStep("Fail", "The Browser " + browser + " not Launched ");

		}
		/*
		 * finally { takeSnap(); }
		 */ }

	public WebElement locateElement(String locator, String locValue) {
		try {
			switch (locator) {
			case "id":
				return driver.findElementById(locValue);
			case "class":
				return driver.findElementByClassName(locValue);
			case "xpath":
				return driver.findElementByXPath(locValue);
			case "Name":
				return driver.findElementByName(locValue);
			case "TagName":
				return driver.findElementByTagName(locValue);
			case "PartialLinkText":
				return driver.findElementByPartialLinkText(locValue);
			case "LinkText":
				return driver.findElementByLinkText(locValue);
			}
			reportStep("Pass", "The Locator Identified the element using " + locator + "Correctly");
		} catch (NoSuchElementException e) {
			// System.out.println("The Element Is Not Located ");
			reportStep("Fail", "The Locator is not Identified the element by " + locator);
		}
		return null;
	}

	public List<WebElement> locateElements(String locator, String locValue) {
		try {
			switch (locator) {
			case "id":
				return driver.findElementsById(locValue);
			case "class":
				return driver.findElementsByClassName(locValue);
			case "xpath":
				return driver.findElementsByXPath(locValue);
			case "Name":
				return driver.findElementsByName(locValue);
			case "TagName":
				return driver.findElementsByTagName(locValue);
			case "PartialLinkText":
				return driver.findElementsByPartialLinkText(locValue);
			case "LinkText":
				return driver.findElementsByLinkText(locValue);
			}
			reportStep("Pass", "The findElements Locator is Identified the element using " + locator + "Correctly");
		} catch (NoSuchElementException e) {
			// System.out.println("The Element Is Not Located ");
			reportStep("Fail", "The findElements Locator is not Identified the element using " + locator);
			throw new RuntimeException();
		}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		try {
			 WebElement elefind = driver.findElementById(locValue);
			reportStep("Pass","Element identified");//How to use for this case
return elefind;
		} catch (NoSuchElementException e) {
			reportStep("Fail", "Element not identified");

		}
		return null;
	}

	public void table(WebElement ele, String locValue, String date) {
		try {
			List<WebElement> findTr = ele.findElements(By.xpath(locValue));// To view tr value
			for (int i = 0; i < findTr.size(); i++) {
				List<WebElement> findTd = findTr.get(i).findElements(By.tagName("td"));
				for (int j = 0; j < findTr.size(); j++) {
					String oneValue = findTd.get(j).getText();
					// WebElement disabled = driver.findElementByXPath("//td[contains(@class,'day
					// othermonth')]");
					if (String.valueOf(oneValue).contentEquals(date)) {
						// findTd.get(j).click();
						click(findTd.get(j));
						// System.out.println("Date Selected Correctly");
					}
				}
			}

			reportStep("Pass", "Value Selected from Table ");
		} catch (NoSuchElementException e) {
			reportStep("Fail", "Value not Selected from Table ");
		}

	}

	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			// System.out.println("The Data " + data + " is Entered Successfully");
			reportStep("Pass", "The Data" + data + " is Entered Successfully");
		} catch (Exception e) {
			reportStep("Fail", "The Data" + data + " is not Entered");
		}
		/*
		 * finally { takeSnap(); }
		 */
	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			// System.out.println("The Element " + ele + " Clicked Successfully");
			reportStep("Pass", "The Element " + ele + " Clicked Successfully");
			takeSnap();

		} catch (Exception e) {
			reportStep("Fail", "The Element " + ele + "Not Clicked");
		}

	}

	public void clickWithoutSnap(WebElement ele) {
		ele.click();
		System.out.println("The Element " + ele + " Clicked Successfully");
	}

	@Override
	public String getText(WebElement ele) {

		try {
			String text = ele.getText();
			reportStep("Pass", "Received the text");
			return text;
		} catch (Exception e) {
			reportStep("Fail", "Not Received the text");

		}
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			// System.out.println("The DropDown Is Selected with " + value);
			reportStep("Pass", "The DropDown Is Selected with " + value);
		} catch (Exception e) {
			reportStep("Fail", "The DropDown Is not Selected");

		}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
     String textOfElement = ele.getText();
     if(textOfElement.contentEquals(expectedText))
     {
    	 reportStep("Pass","Element Verified Correctly");
     }
     else
     {
    	 reportStep("Fail","Element not Verified");

     }
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		String textOfElement = ele.getText();
	     if(textOfElement.contains(expectedText))
	     {
	    	 reportStep("Pass","Element Verified Correctly");
	     }
	     else
	     {
	    	 reportStep("Fail","Element not Verified");

	     }

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> allWindows = driver.getWindowHandles();
			List<String> listOfWindow = new ArrayList<String>();
			listOfWindow.addAll(allWindows);
			driver.switchTo().window(listOfWindow.get(index));
			/*
			 * for (int i = 0; i < listOfWindow.size(); i++) {
			 * 
			 * if(i==0) { driver.switchTo().window(listOfWindow.get(i)); String title =
			 * driver.getTitle(); System.out.println("Main Window " + title);
			 * 
			 * } else { driver.switchTo().window(listOfWindow.get(i)); String title =
			 * driver.getTitle(); System.out.println("Child Window " + title);
			 * 
			 * }
			 * 
			 * } driver.switchTo().window(listOfWindow.get(index));
			 * System.out.println("The Window is Switched "); //Thread.sleep(1000);
			 * //driver.switchTo().window(parentWin);
			 */
			reportStep("Pass", "Window successfully Switched");
		} catch (NoSuchWindowException | ArrayIndexOutOfBoundsException e) {
			reportStep("Fail", "Window not Switched");
		}
	}

	public void mainWind() {
		String parentWin = driver.getWindowHandle();
		driver.switchTo().window(parentWin);

	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void acceptAlert() {
		try {

			driver.switchTo().alert().accept();
			reportStep("Pass", "Alert Handled correctly");
		} catch (NoAlertPresentException e) {
			reportStep("Fail", "Alert Not Handled ");
		}
	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
			reportStep("Pass", "Alert dismissed correctly");

		} catch (NoAlertPresentException e) {
			reportStep("Fail", "Alert Not dismissed ");
		}
	}

	@Override
	public String getAlertText() {
		try {
			String alertMessage = driver.switchTo().alert().getText();
		//System.out.println("Alert Message is = " + alertMessage);
			reportStep("Pass","Alert Message is = " + alertMessage);

		} catch (Exception e) {
			reportStep("Fail","Getting Alert Message is Faild ");
		}
		return null;
	}

	@Override
	public void takeSnap() {
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File dsc = new File("./drivers/img" + i + ".png");
			FileUtils.copyFile(src, dsc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		try {
			driver.close();
			//System.out.println("Browser closed successfully");
          reportStep("Pass","Browser closed successfully");
		} catch (Exception e) {
	          reportStep("Fail","Browser not closed");
		}
	}

	@Override
	public void closeAllBrowsers() {
		try {
			driver.quit();
			//System.out.println("All the Browser closed successfully");
          reportStep("Pass","ALL the Browser closed successfully");
		} catch (Exception e) {
	          reportStep("Fail","ALL Browsers not closed");
		}
	}

}
