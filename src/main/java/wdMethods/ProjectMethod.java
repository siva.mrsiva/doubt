package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

public class ProjectMethod extends SeMethods{
	//@BeforeMethod(groups={"any"})
	@Parameters({"urlFromXML","uNameFromXML","passwordFromXML"})
	@BeforeMethod(groups={"smoke"})
	public void login(String geturl,String getusername,String getpass)
	{
		//beforeMethod();
		startApp("chrome", geturl);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName,getusername);
		WebElement elePassword = locateElement("password");
		type(elePassword, getpass);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCrm = locateElement("LinkText", "CRM/SFA");
		click(eleCrm);

	}
	@AfterMethod(groups={"any"})
	public void close()
	{
		closeBrowser();
	}
	/*@BeforeMethod
	public void starTest() {
		
	}*/
	/*@AfterSuite(groups={"any"})
	public void endTest() {
		endResult();

	}
	@BeforeSuite(groups={"any"})
	public void starttestCase()
	{
		startResult();
	}
*/	}


