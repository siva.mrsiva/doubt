package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;


public class WindowHandking {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		String mainWin = driver.getWindowHandle();
		driver.findElementByLinkText("Contact Us").click();
		Set<String> allWinSet = driver.getWindowHandles();
		List<String> allWinList = new ArrayList<>();
		allWinList.addAll(allWinSet);
		driver.switchTo().window(allWinList.get(1));
		System.out.println(driver.getTitle());
		driver.switchTo().window(allWinList.get(0));
		/*for (String childWin : driver.getWindowHandles()) {
			if (!mainWin.equals(childWin)) {
				driver.switchTo().window(childWin);
				driver.close();
			}
		}*/
		File a = driver.getScreenshotAs(OutputType.FILE);
		File save=new File("./drivers/image.jpg");
		FileUtils.copyFile(a,save);


		driver.close();

	}

}
