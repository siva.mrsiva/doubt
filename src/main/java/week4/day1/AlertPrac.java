package week4.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class AlertPrac {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		driver.switchTo().alert().sendKeys("aaa");
		driver.switchTo().alert().accept();
		String verification = driver.findElementById("demo").getText();
		System.out.println(verification);
		if (verification.contains("Siva")) {
			System.out.println("Verification success");
						
		}
		else
		{
			System.out.println("Verification Failed");
		}
		driver.switchTo().defaultContent();
		driver.findElementById("tryhome").click();	}

}
