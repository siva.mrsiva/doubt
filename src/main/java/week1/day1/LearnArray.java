package week1.day1;

import java.util.Scanner;

public class LearnArray {

	public static void main(String[] args) {
		//Static array
				int age[] = { 35, 45, 50, 60 };
		for (int i = 0; i < age.length; i++) {
			System.out.println(age[i]);
		}
		dynArray();
		oddEven();
	}
	

	public static void dynArray() {
		int rollno[] = new int[6];
		Scanner rn = new Scanner(System.in);
		for (int i = 0; i < rollno.length; i++) {
			System.out.println("Enter the number for " + i + " value=");
			rollno[i] = rn.nextInt();
		}
		for (int i = 0; i < rollno.length; i++) {
			System.out.println(rollno[i]);
		}
	}

	public static void oddEven() {
		int number[] = { 1, 3, 4, 8, 5 };
		int odd=0;
		//for(int i : number) {
			for (int i = 0; i < number.length; i++)
			{
			if(number[i]%2==0)
			{
				System.out.println("Even number is " + i);	
			}
			else
			{
				odd=odd+number[i];
				System.out.println("Odd number is " + i);

			}
		}
	}
}
