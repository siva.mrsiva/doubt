package week2.day1;

public class PocketRadio extends Radio {

	public static void main(String[] args) {
//Radio obj=new Radio();// can not initiate the type error will come because will not able to create obj for abstract
		PocketRadio obj = new PocketRadio();
		obj.changeBattery();
		obj.playStation();
	}

	@Override
	public void changeBattery() {
		System.out.println("Radio class Abstracte method is implemented by PocketRadio ");
	}

	@Override
	public void change() {
		System.out.println("Radio class Abstracte method is implemented by PocketRadio ");
		
	}

}
