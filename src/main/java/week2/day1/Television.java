package week2.day1;

public class Television implements Entertainment {

	public void volume() {
		System.out.println("Volume of the television");
	}

	public void tunner() {
		System.out.println("Channel of the television");
	}

	public void power() {
		System.out.println("Television switch on");
	}

	@Override
	public void sunGroup() {
		System.out.println("Sun is Implemented with refers to entainment");

	}

	@Override
	public void hotStar() {
		// TODO Auto-generated method stub
		System.out.println("hot star is Implemented with refers to entainment");

	}

	@Override
	public void vijayTv() {
		// TODO Auto-generated method stub
		System.out.println("vijay is Implemented with refers to entainment");

	}

	public static void main(String[] args) {
		Television telobj = new Television();
		System.out.println("=======================");
		System.out.println("Belows are the implemented methods in the Television class");
		System.out.println("=======================");
		telobj.power();// methods of television class
		telobj.volume();// methods of television class
		telobj.tunner();// methods of television class
		telobj.sunGroup();// implementation method of Entertainment interface class
		telobj.hotStar();// implementation method of Entertainment interface class
		telobj.vijayTv();// implementation method of Entertainment interface class
		telobj.vijayTv();// implementation method of Entertainment interface class
		System.out.println("=======================");
		System.out.println("Belows are the implemented methods in the smart class");
		System.out.println("=======================");
		SmartTv smaobj = new SmartTv();
		smaobj.bluetooth();// methods of smart class
		smaobj.browse();// methods of smart class
		smaobj.usb();// methods of smart class
		smaobj.volume();
		System.out.println("=======================");
		Television overobj =new SmartTv();
		overobj.volume();
		System.out.println("=======================");
		System.out.println("=======================");
        System.out.println("Below are the implemented methods in television which is defined in the interface");
		Entertainment intobj = new Television();// show only the implementation methods which is defined in the
		System.out.println("=======================");
		intobj.hotStar();
		intobj.sunGroup();
		intobj.vijayTv();

	}
}
